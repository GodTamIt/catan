import curses
import random
import sys


def display_loop(screen):
    while True:
        screen.clear()

        max_y, max_x = screen.getmaxyx()

        # Randomly roll two dice
        die1 = random.randint(1, 6)
        die2 = random.randint(1, 6)

        disp_str = "{}".format(die1 + die2)
        disp_y = max((max_y // 2), 0)
        disp_x = max((max_x // 2) - (len(disp_str) // 2), 0)

        screen.addstr(disp_y, disp_x, disp_str)
        screen.refresh()

        key = screen.getch()
        if key == 27:
            # Exit if ESC key was pressed
            break
        else:
            curses.flash()


def main():
    screen = curses.initscr()
    curses.curs_set(0)
    curses.noecho()
    curses.cbreak()

    try:
        display_loop(screen)
    finally:
        curses.curs_set(1)
        sys.exit(0)


if __name__ == '__main__':
    main()
